#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: GauchoCAD 0.0.78\n"
"POT-Creation-Date: 2020-08-07 14:44 UTC\n"
"PO-Revision-Date: 2020-08-10 22:23+CEST\n"
"Last-Translator: trkSoft bmk@gaucho\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "GauchoCAD"
msgstr "GauchoCAD"

#: .project:2
msgid "A little CAD made in Gambas"
msgstr "Gambas ile yapılmış küçük bir CAD"

#: APPMain.class:192 FLayers.class:64 FLayers1.class:69 dbs.module:1735
msgid "Color"
msgstr "Renk"

#: APPMain.class:645
msgid "Not implemented yet"
msgstr "Henüz uygulanmadı"

#: APPMain.class:656
msgid "Open file"
msgstr "Açık dosya"

#: APPMain.class:758
msgid "Configuration"
msgstr "Yapılandırma"

#: APPMain.class:971
msgid "All"
msgstr "Herşey"

#: APPMain.class:1120
msgid "varies"
msgstr "değişir"

#: APPMain.form:108 FCAD.class:955
msgid "File"
msgstr "Dosya"

#: APPMain.form:112
msgid "New"
msgstr "Yeni"

#: APPMain.form:119
msgid "Open"
msgstr "Açık"

#: APPMain.form:126 FAlias.form:62 FLayers.form:60 FLayers1.form:60
msgid "Save"
msgstr "Kayıt etmek"

#: APPMain.form:133
msgid "Save as"
msgstr "Farklı kaydet"

#: APPMain.form:140
msgid "Import"
msgstr "İthalat"

#: APPMain.form:147
msgid "Export"
msgstr "İhracat"

#: APPMain.form:154
msgid "Plot"
msgstr "Arsa"

#: APPMain.form:161
msgid "Close"
msgstr "Kapat"

#: APPMain.form:170 FConfig.form:66
msgid "Quit"
msgstr "çıkmak"

#: APPMain.form:177
msgid "Options"
msgstr "Seçenekler"

#: APPMain.form:181 FConfig.form:28
msgid "Preferences"
msgstr "Tercihler"

#: APPMain.form:188
msgid "Entities"
msgstr "Varlıkları"

#: APPMain.form:193
msgid "Line"
msgstr "Hat"

#: APPMain.form:200 cadPLine.class:27
msgid "Polyline"
msgstr "Çoklu çizgi"

#: APPMain.form:207
msgid "Circle"
msgstr "Daire"

#: APPMain.form:214
msgid "Arc"
msgstr "yay"

#: APPMain.form:221
msgid "Ellipse"
msgstr "Elips"

#: APPMain.form:228 cadSolid.class:27
msgid "Solid"
msgstr "Katı"

#: APPMain.form:235
msgid "Text"
msgstr "Metin"

#: APPMain.form:242
msgid "Multi Text"
msgstr "Çoklu Metin"

#: APPMain.form:249 FBlock.form:129 cadBlock.class:25
msgid "Block"
msgstr "Blok"

#: APPMain.form:256
msgid "Dimmension"
msgstr "Dimmension"

#: APPMain.form:263
msgid "Ledader"
msgstr "Ledader"

#: APPMain.form:270
msgid "Spline"
msgstr "kama"

#: APPMain.form:277 cadHatch.class:27
msgid "Hatch"
msgstr "kapak"

#: APPMain.form:284
msgid "Rectangle"
msgstr "Dikdörtgen"

#: APPMain.form:290
msgid "Polygon"
msgstr "Çokgen"

#: APPMain.form:297
msgid "Point"
msgstr "Nokta"

#: APPMain.form:304
msgid "Tools"
msgstr "Araçlar"

#: APPMain.form:308
msgid "Move"
msgstr "Hareket"

#: APPMain.form:315 cadCopy.class:36
msgid "Copy"
msgstr "kopya"

#: APPMain.form:322 FBlock.form:45 FHatch.form:39 cadScale.class:38
msgid "Scale"
msgstr "ölçek"

#: APPMain.form:329 cadRotate.class:40
msgid "Rotate"
msgstr "Döndürme"

#: APPMain.form:336 cadMirror.class:40
msgid "Mirror"
msgstr "Ayna"

#: APPMain.form:343 cadTrim.class:38
msgid "Trim"
msgstr "Trim"

#: APPMain.form:350
msgid "Fillet"
msgstr "Fileto"

#: APPMain.form:357
msgid "Chamfer"
msgstr "oluk"

#: APPMain.form:364 cadArray.class:47
msgid "Array"
msgstr "Dizi"

#: APPMain.form:371
msgid "Stretch"
msgstr "Uzatmak"

#: APPMain.form:378
msgid "Offset"
msgstr "dengelemek"

#: APPMain.form:385 cadDivide.class:41
msgid "Divide"
msgstr "bölmek"

#: APPMain.form:392 cadBreak.class:41
msgid "Break"
msgstr "mola"

#: APPMain.form:399 cadErase.class:35
msgid "Delete"
msgstr "Sil"

#: APPMain.form:406
msgid "Structures"
msgstr "Yapılar"

#: APPMain.form:410
msgid "Slab 4 sided"
msgstr "Plaka 4 taraflı"

#: APPMain.form:418
msgid "Slab 3 sided"
msgstr "Plaka 3 taraflı"

#: APPMain.form:425
msgid "Column"
msgstr "sütun"

#: APPMain.form:432
msgid "Beam"
msgstr "kiriş"

#: APPMain.form:439
msgid "Wall column"
msgstr "Duvar sütunu"

#: APPMain.form:447
msgid "View"
msgstr "Görünüm"

#: APPMain.form:451
msgid "Zoom window"
msgstr "Yakınlaştırma penceresi"

#: APPMain.form:458
msgid "Pan view"
msgstr "Yatay kaydırma görünümü"

#: APPMain.form:465
msgid "Zoom extents"
msgstr "Yakınlaştırma kapsamı"

#: APPMain.form:476 FAbout.class:187
msgid "About"
msgstr "hakkında"

#: APPMain.form:483
msgid "Help"
msgstr "Yardım"

#: APPMain.form:530 FLayers.class:94 FLayers1.class:97 cadLayers.class:36
msgid "Layers"
msgstr "Katmanlar"

#: APPMain.form:544
msgid "Line widths"
msgstr "Çizgi genişlikleri"

#: APPMain.form:552
msgid "Dimmension styles"
msgstr "Boyut stilleri"

#: APPMain.form:618
msgid " Smart entities"
msgstr " Akıllı varlıklar"

#: APPMain.form:637
msgid "Refresh"
msgstr "Yenile"

#: APPMain.form:677 FAlias.class:42
msgid "Command"
msgstr "komuta"

#: FAbout.class:128
msgid "Source code"
msgstr "Kaynak kodu"

#: FAbout.class:137
msgid "Contact"
msgstr "İletişim"

#: FAbout.class:155
msgid "Program license"
msgstr "Program lisansı"

#: FAbout.class:165
msgid "The icon does not exist"
msgstr "Simge mevcut değil"

#: FAbout.class:207
msgid "Hello developers of"
msgstr "Merhaba geliştiriciler"

#: FAlias.class:42 cadAlias.class:62
msgid "Alias"
msgstr "takma ad"

#: FAlias.form:69 FBlock.form:73 FHatch.form:67 FLayers.form:68
#: FLayers1.form:68 Form1.form:33
msgid "Cancel"
msgstr "İptal etmek"

#: FBlock.form:31
msgid "Block insertion"
msgstr "Eklemeyi engelle"

#: FBlock.form:39
msgid "Preview"
msgstr "Ön izleme"

#: FBlock.form:63 FHatch.form:57
msgid "Rotation"
msgstr "rotasyon"

#: FBlock.form:68
msgid "Insert"
msgstr "Ekle"

#: FBlock.form:122
msgid "Folder"
msgstr "Klasör"

#: FBlock.form:136
msgid "Parameters"
msgstr "Parametreler"

#: FCAD.class:959 uty.module:554
msgid "Version"
msgstr "versiyon"

#: FCAD.form:113
msgid "Coords"
msgstr "coords"

#: FConfig.class:93
msgid "Configuration file does not exist"
msgstr "Yapılandırma dosyası mevcut değil"

#: FConfig.class:492
msgid "The changes was saved"
msgstr "Değişiklikler kaydedildi"

#: FConfig.class:512
msgid "A new copy was created"
msgstr "Yeni bir kopya oluşturuldu"

#: FConfig.class:525
msgid "A copy was successfully archived"
msgstr "Bir kopya başarıyla arşivlendi"

#: FConfig.class:534
msgid "Restored configuration"
msgstr "Yapılandırma geri yüklendi"

#: FConfig.form:32
msgid "New preferences"
msgstr "Yeni tercihler"

#: FConfig.form:39
msgid "Apply preferences changes"
msgstr "Tercih değişikliklerini uygula"

#: FConfig.form:46
msgid "Restore preferences"
msgstr "Tercihleri ​​geri yükle"

#: FConfig.form:53
msgid "Archive preferences"
msgstr "Arşiv tercihleri"

#: FConfig.form:59
msgid "Open log file"
msgstr "Günlük dosyasını aç"

#: FConfig.form:113
msgid "Open application directory"
msgstr "Uygulama dizinini aç"

#: FConfig.form:143
msgid "Directories"
msgstr "Dizinler"

#: FHatch.form:34
msgid "Pattern"
msgstr "Desen"

#: FHatch.form:62 Form1.form:27
msgid "OK"
msgstr "tamam"

#: FHatch.form:72
msgid "Add areas"
msgstr "Alan ekleyin"

#: FHatch.form:77
msgid "Remove areas"
msgstr "Alanları kaldır"

#: FInspector.class:18
msgid "Length"
msgstr "uzunluk"

#: FInspector.class:29
msgid "Radious"
msgstr "radious"

#: FInspector.form:23
msgid "Layer"
msgstr "Katman"

#: FLayers.class:64 FLayers1.class:69
msgid "Frozen"
msgstr "Dondurulmuş"

#: FLayers.class:64 FLayers1.class:69
msgid "Lock"
msgstr "Kilit"

#: FLayers.class:64 FLayers1.class:69 dbs.module:1716
msgid "Name"
msgstr "ad"

#: FLayers.class:64 FLayers1.class:69
msgid "Print"
msgstr "Yazdır"

#: FLayers.class:64 FLayers1.class:69
msgid "Show"
msgstr "Göstermek"

#: FLayers.class:64 FLayers1.class:69
msgid "Style"
msgstr "stil"

#: FLayers.class:64 FLayers1.class:69
msgid "Width"
msgstr "Genişlik"

#: FLayers.class:64 FLayers1.class:69
msgid "id"
msgstr "İD"

#: Starter.module:275
msgid "Red"
msgstr "Kırmızı"

#: Starter.module:276
msgid "Yellow"
msgstr "Sarı"

#: Starter.module:277
msgid "Green"
msgstr "Yeşil"

#: Starter.module:278
msgid "Cyan"
msgstr "Cam göbeği"

#: Starter.module:279
msgid "Blue"
msgstr "Mavi"

#: Starter.module:280
msgid "Magenta"
msgstr "eflatun"

#: Starter.module:281
msgid "White"
msgstr "Beyaz"

#: _cadImage.class:26
msgid "Inserts an image"
msgstr "Bir görüntü ekler"

#: cad.module:2220 xmg.module:681
msgid "Program"
msgstr "program"

#: cad.module:2224 xmg.module:685
msgid "Application directory"
msgstr "Uygulama dizini"

#: cad.module:2234 xmg.module:695
msgid "Window background color"
msgstr "Pencere arka plan rengi"

#: cad.module:2246 xmg.module:707
msgid "Desktop"
msgstr "Masaüstü"

#: cad.module:2250 xmg.module:711
msgid "Simple text editor by default"
msgstr "Varsayılan olarak basit metin düzenleyici"

#: cad.module:2260 xmg.module:721
msgid "Program icons"
msgstr "Program simgeleri"

#: cad.module:2270
msgid "Drawing window dark mode"
msgstr "Çizim penceresi karanlık modu"

#: cad.module:2279
msgid "Draw originals"
msgstr "Orijinalleri çiz"

#: cad.module:2288
msgid "Draw marked"
msgstr "Beraberlik işaretlendi"

#: cad.module:2297
msgid "Draw only columns"
msgstr "Yalnızca sütunlar çizin"

#: cad.module:2306
msgid "Draw bounds"
msgstr "Sınır çizin"

#: cad.module:2315
msgid "Dimensions format"
msgstr "Boyut biçimi"

#: cad.module:2324
msgid "Color sellection"
msgstr "Renk seçimi"

#: cad.module:2333
msgid "Tool active"
msgstr "Araç aktif"

#: cad.module:2342
msgid "Orthogonal"
msgstr "Dikey"

#: cad.module:2351
msgid "Orthogonal forced"
msgstr "Ortogonal zorunlu"

#: cad.module:2360
msgid "Orthogonal ignored"
msgstr "Ortogonal yok sayıldı"

#: cad.module:2369
msgid "Show inspector"
msgstr "Denetçiyi göster"

#: cadArc.class:26
msgid "Draws an arc"
msgstr "Yay çizer"

#: cadArc.class:155
msgid " to "
msgstr " için"

#: cadArea.class:38
msgid "Area"
msgstr "alan"

#: cadArea.class:38 cadRuler.class:38
msgid "mark points"
msgstr "işaret noktaları"

#: cadArea.class:99 cadArray.class:102 cadChamfer.class:60 cadDim.class:474
#: cadFillet.class:60 cadMirror.class:172 cadOffset.class:269
#: cadProtractor.class:110 cadRotate.class:174 cadRuler.class:98
msgid "Bad input"
msgstr "Hatalı giriş"

#: cadArea.class:109
msgid "Perimeter"
msgstr "Çevre"

#: cadArray.class:54 cadCopy.class:43 cadErase.class:42 cadMirror.class:47
#: cadMove.class:43 cadRotate.class:47 cadScale.class:45 cadStretch.class:48
msgid "Select entities"
msgstr "Varlıkları seçin"

#: cadArray.class:61 cadCopy.class:50 cadMirror.class:55 cadMove.class:50
#: cadRotate.class:54 cadScale.class:52 cadStretch.class:56
msgid "Base point"
msgstr "Taban noktası"

#: cadArray.class:62 cadCopy.class:51 cadMirror.class:56 cadMove.class:51
#: cadRotate.class:55 cadScale.class:53 cadStretch.class:57 cadTrim.class:152
#: clsDefaultJob.class:149
msgid "elements"
msgstr "elementler"

#: cadArray.class:81
msgid "Orthogonal or polar"
msgstr "Ortogonal veya polar"

#: cadArray.class:95
msgid "Columns"
msgstr "Sütunlar"

#: cadArray.class:99
msgid "Repetitions"
msgstr "tekrarlar"

#: cadArray.class:110
msgid "Distance"
msgstr "Mesafe"

#: cadArray.class:114 cadMirror.class:83 cadProtractor.class:85
#: cadRotate.class:82
msgid "Angle"
msgstr "Açı"

#: cadArray.class:122
msgid "Rows"
msgstr "Satırlar"

#: cadBeam.class:30
msgid "Insert a beam"
msgstr "Bir kiriş ekleyin"

#: cadBlock.class:65
msgid "Select two or mores entities first"
msgstr "Önce iki veya daha fazla varlık seçin"

#: cadBreak.class:48 cadDivide.class:48
msgid "Select only one entity"
msgstr "Yalnızca bir varlık seçin"

#: cadBreak.class:59
msgid "Breakpoint of entity"
msgstr "Varlığın kesme noktası"

#: cadBreak.class:63 cadDivide.class:63
msgid "Can't break that entity"
msgstr "O varlığı kıramazsın"

#: cadBreak.class:85 cadDivide.class:85
msgid "Invalid breakpoint"
msgstr "Geçersiz kesme noktası"

#: cadBreak.class:119 cadDivide.class:99 cadOffset.class:74
msgid "Can't offset that entity"
msgstr "Bu varlığı dengeleyemez"

#: cadCWall.class:30
msgid "Insert a wall column"
msgstr "Bir duvar sütunu ekleyin"

#: cadChamfer.class:47
msgid "enter size"
msgstr "beden girin"

#: cadChamfer.class:62 cadFillet.class:62
msgid "Select first line"
msgstr "İlk satırı seçin"

#: cadChamfer.class:98 cadFillet.class:98
msgid "Select second line"
msgstr "İkinci satırı seçin"

#: cadCircle.class:27
msgid "Draw a circle"
msgstr "Bir daire çizin"

#: cadColumn.class:31
msgid "Insert a column"
msgstr "Bir sütun ekleyin"

#: cadCopy.class:68 cadMove.class:72 cadScale.class:70 cadStretch.class:72
msgid "Final point"
msgstr "Son nokta"

#: cadDim.class:39
msgid "Insert an acotation"
msgstr "Bir ekotasyon ekle"

#: cadDivide.class:59
msgid "First breakpoint of entity"
msgstr "Varlığın ilk kesme noktası"

#: cadDivide.class:94
msgid "Second breakpoint of entity"
msgstr "Varlığın ikinci kesme noktası"

#: cadEllipse.class:27
msgid "Draw an ellipse"
msgstr "Elips çizin"

#: cadExplode.class:34
msgid "Explode"
msgstr "patlamak"

#: cadExplode.class:41
msgid "Select blocks"
msgstr "Blokları seçin"

#: cadFillet.class:47
msgid "enter radious"
msgstr "ışıl ışıl girmek"

#: cadInsert.class:25
msgid "Inserts a block"
msgstr "Bir blok ekler"

#: cadLeader.class:27
msgid "Leader"
msgstr "Önder"

#: cadLine.class:27 cadRectangle.class:27
msgid "a line"
msgstr "bir çizgi"

#: cadLoad.class:32 cadLoadBIM.class:32
msgid "Files"
msgstr "Dosyalar"

#: cadMText.class:28
msgid "Inserts a multiline text"
msgstr "Çok satırlı bir metin ekler"

#: cadMirror.class:109
msgid "Keep originals"
msgstr "Orijinalleri saklayın"

#: cadMove.class:35
msgid "Move "
msgstr "Hareket"

#: cadOffset.class:45
msgid "enter distance"
msgstr "mesafeyi girin"

#: cadOffset.class:70
msgid "Side to offset"
msgstr "Yan ofset"

#: cadOffset.class:149
msgid "select an entity"
msgstr "bir varlık seçin"

#: cadOffset.class:236
msgid "enter a valid text size"
msgstr "geçerli bir metin boyutu girin"

#: cadPLine.class:29 cadSPLine.class:12
msgid "Next Point"
msgstr "Sonraki Nokta"

#: cadPan.class:30
msgid "Panning"
msgstr "Yatay kaydırma"

#: cadPolygon.class:27
msgid "a polygon"
msgstr "bir çokgen"

#: cadProtractor.class:38
msgid "mark first point"
msgstr "ilk noktayı işaretle"

#: cadProtractor.class:60
msgid "mark center"
msgstr "merkezi işaretle"

#: cadRuler.class:73
msgid "Total length"
msgstr "Toplam uzunluk"

#: cadSlab.class:31
msgid "Insert a slab"
msgstr "Bir levha yerleştirin"

#: cadSlabT.class:32
msgid "Insert a triangular slab"
msgstr "Üçgen bir levha yerleştirin"

#: cadStretch.class:41
msgid "Stretch "
msgstr "Uzatmak"

#: cadText.class:28
msgid "Inserts a text"
msgstr "Bir metin ekler"

#: cadTrim.class:46
msgid "Select cutting edges"
msgstr "Kesme kenarlarını seçin"

#: cadTrim.class:59
msgid "entities to cut"
msgstr "kesilecek varlıklar"

#: cadTrim.class:152 clsDefaultJob.class:149
msgid "Selected"
msgstr "seçilmiş"

#: cadZoomW.class:75
msgid "Window is too small"
msgstr "Pencere çok küçük"

#: clsEntities.class:59
msgid "Loading blocks "
msgstr "Yükleme blokları"

#: clsEntityBuilder.class:91
msgid "Can create entity"
msgstr "Varlık oluşturabilir"

#: dbs.module:1378
msgid "Extracting data from"
msgstr "Verilerin çıkarılması"

#: dbs.module:1378
msgid "System"
msgstr "sistem"

#: dbs.module:1385
msgid "Formatting data from"
msgstr "Verileri biçimlendirme"

#: dbs.module:1393
msgid "Database completed"
msgstr "Veritabanı tamamlandı"

#: dbs.module:1472
msgid "Table and view field titles"
msgstr "Tablo ve alan başlıklarını görüntüle"

#: dbs.module:1474
msgid "View titles"
msgstr "Başlıkları görüntüle"

#: dbs.module:1554
msgid "View field titles"
msgstr "Alan başlıklarını görüntüleyin"

#: dbs.module:1714
msgid "Date"
msgstr "tarih"

#: dbs.module:1717
msgid "Email"
msgstr "E-posta"

#: dbs.module:1718
msgid "Mobile"
msgstr "seyyar"

#: dbs.module:1719
msgid "Phone"
msgstr "Telefon"

#: dbs.module:1722
msgid "Quantity"
msgstr "miktar"

#: dbs.module:1723
msgid "Price"
msgstr "Fiyat"

#: dbs.module:1724
msgid "Trademark"
msgstr "Marka"

#: dbs.module:1725
msgid "Description"
msgstr "Açıklama"

#: dbs.module:1726
msgid "Address"
msgstr "Adres"

#: dbs.module:1727 uty.module:553
msgid "Vendor"
msgstr "SATICI"

#: dbs.module:1728
msgid "Class"
msgstr "Sınıf"

#: dbs.module:1729
msgid "Code"
msgstr "kod"

#: dbs.module:1730
msgid "City"
msgstr "Kent"

#: dbs.module:1731
msgid "Symbol"
msgstr "sembol"

#: dbs.module:1732
msgid "Job"
msgstr "İş"

#: dbs.module:1733
msgid "Client"
msgstr "müşteri"

#: dbs.module:1734
msgid "Type"
msgstr "tip"

#: dsk.module:110
msgid "Select File"
msgstr "Dosya Seç"

#: dsk.module:119
msgid "Filter"
msgstr "filtre"

#: dsk.module:141
msgid "Select directory"
msgstr "Dizini seçin"

#: dsk.module:196
msgid "Hi"
msgstr "Selam"

#: modUtils.module:256
msgid "Can't determine array type"
msgstr "Dizi türünü belirleyemiyorum"

#: sog.module:102
msgid "Unknown"
msgstr "Bilinmeyen"

#: uty.module:552
msgid "Author"
msgstr "Yazar"

#: uty.module:555
msgid "Date of this document"
msgstr "Bu belgenin tarihi"

#: uty.module:556
msgid "Components"
msgstr "Bileşenler"

#: uty.module:558
msgid "It is made up"
msgstr "Uydurulmuş"

#: uty.module:558
msgid "methods"
msgstr "yöntemleri"

#: uty.module:558
msgid "modules"
msgstr "modüller"

#: vag.module:455
msgid "Spaces at the beginning and at the end"
msgstr "Baştaki ve sondaki boşluklar"

#: vag.module:457
msgid "Space at the start"
msgstr "Başlangıçta boşluk"

#: vag.module:461
msgid "Space at the end"
msgstr "Sonunda boşluk"

#: vag.module:466
msgid "Repeated"
msgstr "tekrarlanan"

#: vag.module:493
msgid "Excluded"
msgstr "Dışlanan"

#: vag.module:497
msgid "More than one capital letter"
msgstr "Birden fazla büyük harf"
